﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Cognition_14.Resources;
using Newtonsoft.Json;
using SQLitePCL;
using Cognition.Common;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Tasks;
using System.Device.Location;
using System.Windows.Media;
using System.Threading;

namespace Cognition_14
{
    public partial class DownloadPage : PhoneApplicationPage
    {
        public ProgressIndicator _progressIndicator;

        public string P_KEY = "_id";
        public String KEY_EVENT_NAME;
        public String KEY_EVENT_TIME = "event_time";
        public String KEY_DAY_NUMBER = "day_number";
        public String KEY_DAY = "day";
        public String KEY_EVENT_DATE = "event_date";
        public String KEY_EVENT_DAY = "event_day";
        public String KEY_EVENT_LEVEL = "event_level";
        public String KEY_EVENT_TYPE = "event_type";
        public String KEY_EVENT_DESC = "event_description";
        public String KEY_EVENT_REG_END_DATE = "event_reg_end_date";
        public String KEY_EVENT_ABS_SUB = "event_abs_submission";
        public String KEY_EVENT_COUNCIL = "event_council";
        public String KEY_CONTACT_ONE = "contact1";
        public String KEY_CONTACT_TWO = "contact2";
        public String KEY_NAME_ONE = "name1";
        public String KEY_PHONE_ONE = "phone1";
        public String KEY_NAME_TWO = "name2";
        public String KEY_PHONE_TWO = "phone2";
        public String KEY_EVENT_REG_LINK = "event_reg_link";

        public DownloadPage()
        {
            InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var get = App.newDB;

            string name;
            using (var check = get.Prepare("SELECT event_name FROM cog_eventdetails"))
            {
                check.Step();
                name = (string)check[0];
            }

            if (name == null)
            {
                try
                {
                    _progressIndicator = new ProgressIndicator
                    {
                        IsIndeterminate = true,
                        Text = " Loading...",
                        IsVisible = true,

                    };
                    SystemTray.SetIsVisible(this, true);
                    SystemTray.SetProgressIndicator(this, _progressIndicator);
                    SystemTray.SetOpacity(this, 0);
                    SystemTray.SetForegroundColor(this, Colors.Black);

                    //Thread.Sleep(4000);

                    string response = await HttpPost.Httppost();

                    dynamic objects = JsonConvert.DeserializeObject(response);

                    foreach (var o in objects)
                    {
                        using (var insert = get.Prepare("INSERT INTO cog_eventdetails (event_name, event_time, day_number, day, event_date, event_day, event_level, event_type, event_description, event_reg_end_date, event_abs_submission, event_council, contact1, contact2, name1, phone1, name2, phone2, event_reg_link) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                        {
                            KEY_EVENT_NAME = o["event_name"];
                            insert.Bind(1, KEY_EVENT_NAME);
                            KEY_EVENT_TIME = o["event_time"];
                            insert.Bind(2, KEY_EVENT_TIME);
                            KEY_DAY_NUMBER = o["day_number"];
                            insert.Bind(3, KEY_DAY_NUMBER);
                            KEY_DAY = o["day"];
                            insert.Bind(4, KEY_DAY);
                            KEY_EVENT_DATE = o["event_date"];
                            insert.Bind(5, KEY_EVENT_DATE);
                            KEY_EVENT_DAY = o["event_day"];
                            insert.Bind(6, KEY_EVENT_DAY);
                            KEY_EVENT_LEVEL = o["event_level"];
                            insert.Bind(7, KEY_EVENT_LEVEL);
                            KEY_EVENT_TYPE = o["event_type"];
                            insert.Bind(8, KEY_EVENT_TYPE);
                            KEY_EVENT_DESC = o["event_description"];
                            insert.Bind(9, KEY_EVENT_DESC);
                            KEY_EVENT_REG_END_DATE = o["event_reg_end_date"];
                            insert.Bind(10, KEY_EVENT_REG_END_DATE);
                            KEY_EVENT_ABS_SUB = o["event_abs_submission"];
                            insert.Bind(11, KEY_EVENT_ABS_SUB);
                            KEY_EVENT_COUNCIL = o["event_council"];
                            insert.Bind(12, KEY_EVENT_COUNCIL);
                            KEY_CONTACT_ONE = o["contact1"];
                            insert.Bind(13, KEY_CONTACT_ONE);
                            KEY_CONTACT_TWO = o["contact2"];
                            insert.Bind(14, KEY_CONTACT_TWO);
                            KEY_NAME_ONE = o["name1"];
                            insert.Bind(15, KEY_NAME_ONE);
                            KEY_PHONE_ONE = o["phone1"];
                            insert.Bind(16, KEY_PHONE_ONE);
                            KEY_NAME_TWO = o["name2"];
                            insert.Bind(17, KEY_NAME_TWO);
                            KEY_PHONE_TWO = o["phone2"];
                            insert.Bind(18, KEY_PHONE_TWO);
                            KEY_EVENT_REG_LINK = o["event_reg_link"];
                            insert.Bind(19, KEY_EVENT_REG_LINK);
                            insert.Step();
                        }
                    }

                    string inter = await HttpPost.HttppostInter();
                    dynamic obj = JsonConvert.DeserializeObject(inter);

                    foreach (var andar in obj)
                    {
                        using (var put = get.Prepare("INSERT INTO cog_eventdetails_inter (event_name, event_time, day_number, day, event_date, event_day, event_type, event_description, event_reg_link, name1, phone1, name2, phone2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                        {
                            KEY_EVENT_NAME = andar["event_name"];
                            put.Bind(1, KEY_EVENT_NAME);
                            KEY_EVENT_TIME = andar["event_time"];
                            put.Bind(2, KEY_EVENT_TIME);
                            KEY_DAY_NUMBER = andar["day_number"];
                            put.Bind(3, KEY_DAY_NUMBER);
                            KEY_DAY = andar["day"];
                            put.Bind(4, KEY_DAY);
                            KEY_EVENT_DATE = andar["event_date"];
                            put.Bind(5, KEY_EVENT_DATE);
                            KEY_EVENT_DAY = andar["event_day"];
                            put.Bind(6, KEY_EVENT_DAY);
                            KEY_EVENT_TYPE = andar["event_type"];
                            put.Bind(7, KEY_EVENT_TYPE);
                            KEY_EVENT_DESC = andar["event_description"];
                            put.Bind(8, KEY_EVENT_DESC);
                            KEY_EVENT_REG_LINK = andar["event_reg_link"];
                            put.Bind(9, KEY_EVENT_REG_LINK);
                            KEY_NAME_ONE = andar["name1"];
                            put.Bind(10, KEY_NAME_ONE);
                            KEY_PHONE_ONE = andar["phone1"];
                            put.Bind(11, KEY_PHONE_ONE);
                            KEY_NAME_TWO = andar["name2"];
                            put.Bind(12, KEY_NAME_TWO);
                            KEY_PHONE_TWO = andar["phone2"];
                            put.Bind(13, KEY_PHONE_TWO);
                            put.Step();
                        }
                    }

                    string intra = await HttpPost.HttppostIntra();
                    dynamic ob = JsonConvert.DeserializeObject(intra);

                    foreach (var bahar in ob)
                    {
                        using (var put = get.Prepare("INSERT INTO cog_eventdetails_intra (event_name, event_time, day_number, day, event_date, event_day, event_type, event_description, event_reg_link, name1, phone1, name2, phone2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                        {
                            KEY_EVENT_NAME = bahar["event_name"];
                            put.Bind(1, KEY_EVENT_NAME);
                            KEY_EVENT_TIME = bahar["event_time"];
                            put.Bind(2, KEY_EVENT_TIME);
                            KEY_DAY_NUMBER = bahar["day_number"];
                            put.Bind(3, KEY_DAY_NUMBER);
                            KEY_DAY = bahar["day"];
                            put.Bind(4, KEY_DAY);
                            KEY_EVENT_DATE = bahar["event_date"];
                            put.Bind(5, KEY_EVENT_DATE);
                            KEY_EVENT_DAY = bahar["event_day"];
                            put.Bind(6, KEY_EVENT_DAY);
                            KEY_EVENT_TYPE = bahar["event_type"];
                            put.Bind(7, KEY_EVENT_TYPE);
                            KEY_EVENT_DESC = bahar["event_description"];
                            put.Bind(8, KEY_EVENT_DESC);
                            KEY_EVENT_REG_LINK = bahar["event_reg_link"];
                            put.Bind(9, KEY_EVENT_REG_LINK);
                            KEY_NAME_ONE = bahar["name1"];
                            put.Bind(10, KEY_NAME_ONE);
                            KEY_PHONE_ONE = bahar["phone1"];
                            put.Bind(11, KEY_PHONE_ONE);
                            KEY_NAME_TWO = bahar["name2"];
                            put.Bind(12, KEY_NAME_TWO);
                            KEY_PHONE_TWO = bahar["phone2"];
                            put.Bind(13, KEY_PHONE_TWO);
                            put.Step();
                        }
                    }

                    string workshop = await HttpPost.HttppostWork();
                    dynamic objct = JsonConvert.DeserializeObject(workshop);

                    foreach (var work in objct)
                    {
                        using (var put = get.Prepare("INSERT INTO cog_eventdetails_workshop(event_name, event_time, day_number, day, event_date, event_day, event_type, event_description, event_reg_link, name1, phone1, name2, phone2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                        {
                            KEY_EVENT_NAME = work["event_name"];
                            put.Bind(1, KEY_EVENT_NAME);
                            KEY_EVENT_TIME = work["event_time"];
                            put.Bind(2, KEY_EVENT_TIME);
                            KEY_DAY_NUMBER = work["day_number"];
                            put.Bind(3, KEY_DAY_NUMBER);
                            KEY_DAY = work["day"];
                            put.Bind(4, KEY_DAY);
                            KEY_EVENT_DATE = work["event_date"];
                            put.Bind(5, KEY_EVENT_DATE);
                            KEY_EVENT_DAY = work["event_day"];
                            put.Bind(6, KEY_EVENT_DAY);
                            KEY_EVENT_TYPE = work["event_type"];
                            put.Bind(7, KEY_EVENT_TYPE);
                            KEY_EVENT_DESC = work["event_description"];
                            put.Bind(8, KEY_EVENT_DESC);
                            KEY_EVENT_REG_LINK = work["event_reg_link"];
                            put.Bind(9, KEY_EVENT_REG_LINK);
                            KEY_NAME_ONE = work["name1"];
                            put.Bind(10, KEY_NAME_ONE);
                            KEY_PHONE_ONE = work["phone1"];
                            put.Bind(11, KEY_PHONE_ONE);
                            KEY_NAME_TWO = work["name2"];
                            put.Bind(12, KEY_NAME_TWO);
                            KEY_PHONE_TWO = work["phone2"];
                            put.Bind(13, KEY_PHONE_TWO);
                            put.Step();
                        }
                    }

                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }
                catch (System.Net.WebException)
                {
                    Thread.Sleep(3000);
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }
            }
            else
            {
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
        }
    }
}