﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Cognition_14.Resources;
using Newtonsoft.Json;
using SQLitePCL;
using Cognition.Common;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Tasks;
using System.Device.Location;
using System.Windows.Media;

namespace Cognition_14
{
    public partial class MainPage : PhoneApplicationPage
    {
        public ProgressIndicator _progressIndicator;
        public String P_KEY = "_id";
        public String KEY_EVENT_NAME;
        public String KEY_EVENT_TIME = "event_time";
        public String KEY_DAY_NUMBER = "day_number";
        public String KEY_DAY = "day";
        public String KEY_EVENT_DATE = "event_date";
        public String KEY_EVENT_DAY = "event_day";
        public String KEY_EVENT_LEVEL = "event_level";
        public String KEY_EVENT_TYPE = "event_type";
        public String KEY_EVENT_DESC = "event_description";
        public String KEY_EVENT_REG_END_DATE = "event_reg_end_date";
        public String KEY_EVENT_ABS_SUB = "event_abs_submission";
        public String KEY_EVENT_COUNCIL = "event_council";
        public String KEY_CONTACT_ONE = "contact1";
        public String KEY_CONTACT_TWO = "contact2";
        public String KEY_NAME_ONE = "name1";
        public String KEY_PHONE_ONE = "phone1";
        public String KEY_NAME_TWO = "name2";
        public String KEY_PHONE_TWO = "phone2";
        public String KEY_EVENT_REG_LINK = "event_reg_link";
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton refresh = new ApplicationBarIconButton();
            refresh.IconUri = new Uri("/Assets/AppBar/refresh.png", UriKind.Relative);
            refresh.Text = "refresh";
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.Buttons.Add(refresh);
            refresh.Click += new EventHandler(refreshClick);
        }

        string response;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();
            }
            base.OnNavigatedTo(e);

            var get = App.newDB;

            Int64 LastID;
            using (var last = get.Prepare("SELECT _id FROM cog_eventdetails ORDER BY _id DESC"))
            {
                last.Step();
                LastID = (Int64)last[0];
            }

            //int ID = Int32.Parse(LastID);

            List<EventList> events = new List<EventList>();
            int i = 1;
            string s = "";
            while (i <= LastID)
            {
                using (var getList = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_end_date, event_abs_submission, contact1, contact2, name1, phone1, name2, phone2, event_reg_link FROM cog_eventdetails WHERE _id = ?"))
                {
                    getList.Bind(1, i);
                    getList.Step();
                    KEY_EVENT_NAME = (string)getList[1];
                    switch (KEY_EVENT_NAME)
                    {
                        case "TechnoKrieg":
                            s = "/Assets/Event/Event_TechnoKrieg.png";
                            break;

                        case "Innovations":
                            s = "/Assets/Event/Event_Innovations.png";
                            break;

                        case "Matrix":
                            s = "/Assets/Event/Event_Matrix.png";
                            break;

                        case "WebWeaWerz":
                            s = "/Assets/Event/Event_WWW.png";
                            break;

                        case "TechXter":
                            s = "/Assets/Event/Event_TechXter.png";
                            break;

                        case "IQ":
                            s = "/Assets/Event/Event_IQ.png";
                            break;

                        case "Squabble":
                            s = "/Assets/Event/Event_Squabble.png";
                            break;

                        case "Hack In The Box":
                            s = "/Assets/Event/Event_Hack.png";
                            break;

                        default:
                            s = null;
                            break;
                    }
                    if (s == null)
                    {
                        ToastPrompt toast1 = new ToastPrompt();
                        toast1.Message = "Please check your internet connection";
                        toast1.Show();
                    }
                    else
                    {
                        events.Add(new EventList() { Event_Name = (string)getList[1], Event_Day = (string)getList[2], Event_Description = (string)getList[3], Event_Reg_End_Date = (string)getList[4], Event_Abs_Submission = (string)getList[5], Contact1 = (string)getList[6], Contact2 = (string)getList[7], Name1 = (string)getList[8], Phone1 = (string)getList[9], Name2 = (string)getList[10], Phone2 = (string)getList[11], Event_Reg_Link = (string)getList[12], img = s });
                    }
                    i++;

                }
            }
            EventsView.ItemsSource = events;

            using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_workshop ORDER BY _id DESC"))
            {
                last.Step();
                LastID = (Int64)last[0];
            }

            List<EventList> InterDB = new List<EventList>();
            int j = 1;
            string h = "";
            while (j <= LastID)
            {
                using (var List = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_link, name1, phone1, name2, phone2 FROM cog_eventdetails_workshop WHERE _id = ?"))
                {
                    List.Bind(1, j);
                    List.Step();
                    KEY_EVENT_NAME = (string)List[1];
                    switch (KEY_EVENT_NAME)
                    {
                        case "Python":
                            h = "/Assets/Workshop/Python_Workshop.png";
                            break;

                        case "Photoshop":
                            h = "/Assets/Workshop/Photoshop_Workshop.png";
                            break;

                        case "Php/SQL":
                            h = "/Assets/Workshop/Php_Workshop.png";
                            break;

                        case "Arduino":
                            h = "/Assets/Workshop/Arduino_Workshop.png";
                            break;

                        default:
                            h = null;
                            break;
                    }
                    if (h == null)
                    {
                        
                    }
                    else
                    {
                        InterDB.Add(new EventList() { Event_Name = (string)List[1], Event_Day = (string)List[2], Event_Description = (string)List[3], Event_Reg_Link = (string)List[4], Name1 = (string)List[5], Phone1 = (string)List[6], Name2 = (string)List[7], Phone2 = (string)List[8], img = h });
                    }
                    j++;
                }
            }
            Workshop.ItemsSource = InterDB;

            using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_intra ORDER BY _id DESC"))
            {
                last.Step();
                LastID = (Int64)last[0];
            }

            List<EventList> IntraDB = new List<EventList>();
            int b = 1;
            string a = "";
            while (b <= LastID)
            {
                using (var List = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_link, name1, phone1, name2, phone2 FROM cog_eventdetails_intra WHERE _id = ?"))
                {
                    List.Bind(1, b);
                    List.Step();
                    KEY_EVENT_NAME = (string)List[1];
                    switch (KEY_EVENT_NAME)
                    {
                        case "Google Whacking":
                            a = "/Assets/Intra/GoogleWhacking_Icon.png";
                            break;

                        case "Code Warz":
                            a = "/Assets/Intra/CodeWars_Icon.png";
                            break;

                        case "PhotoEditor":
                            a = "/Assets/Intra/PhotoEditor_Icon.png";
                            break;

                        case "Technolution":
                            a = "/Assets/Intra/TPP_Icon.png";
                            break;

                        case "BlitzKrieg":
                            a = "/Assets/Intra/Blitzkrieg_Icon.png";
                            break;

                        default:
                            a = null;
                            break;
                    }
                    if (a == null)
                    {

                    }
                    else
                    {
                        IntraDB.Add(new EventList() { Event_Name = (string)List[1], Event_Day = (string)List[2], Event_Description = (string)List[3], Event_Reg_Link = (string)List[4], Name1 = (string)List[5], Phone1 = (string)List[6], Name2 = (string)List[7], Phone2 = (string)List[8], img = a });
                    }
                    b++;
                }
            }
            IntraCollegeEvents.ItemsSource = IntraDB;

            /*using (var last = get.Prepare("SELECT _id FROM cog_news ORDER BY _id DESC"))
            {
                last.Step();
                LastID = (Int64)last[0];
            }

            List<NewsList> NewsDB = new List<NewsList>();
            int c = 1;
            while (c <= LastID)
            {
                using (var List = get.Prepare("SELECT _id, news_name, news_day, event_description FROM cog_news WHERE _id = ?"))
                {
                    List.Bind(1, c);
                    List.Step();

                    NewsDB.Add(new NewsList() { News_Title = (string)List[1], News_Day = (string)List[2], News_Description = (string)List[3] });
                }
                c++;
            }
            NewsView.ItemsSource = NewsDB;*/

            List<InterCollege> inter = new List<InterCollege>();

            inter.Add(new InterCollege() { Misc = "Miscellaneous", img = "/Assets/Inter/Misc_Inter.png" });
            inter.Add(new InterCollege() { Envision = "Envision", img = "/Assets/Inter/Envision_Inter.png" });
            inter.Add(new InterCollege() { Adrenaline = "Adrenaline", img = "/Assets/Inter/Adrenaline_Inter.png" });
            inter.Add(new InterCollege() { Subtle_Creativity = "Subtle", img = "/Assets/Inter/Subtle_Inter.png" });
            InterCollegeEvents.ItemsSource = inter;
        }
        public class EventList
        {
            public string img { get; set; }
            public string Event_Name { get; set; }
            public string Event_Day { get; set; }
            public string Event_Description { get; set; }
            public string Event_Reg_End_Date { get; set; }
            public string Event_Abs_Submission { get; set; }
            public string Contact1 { get; set; }
            public string Contact2 { get; set; }
            public string Name1 { get; set; }
            public string Phone1 { get; set; }
            public string Name2 { get; set; }
            public string Phone2 { get; set; }
            public string Event_Reg_Link { get; set; }
        }

        public class InterCollege
        {
            public string Adrenaline { get; set; }
            public string Misc { get; set; }
            public string Envision { get; set; }
            public string Subtle_Creativity { get; set; }
            public string img { get; set; }

        }

        public class NewsList
        {
            public string News_Title { get; set; }
            public string News_Description { get; set; }
            public string News_Day { get; set; }
        }

        private void onEventExpand(object sender, SelectionChangedEventArgs e)
        {
            EventList id = EventsView.SelectedItem as EventList;
            var _event_name = id.Event_Name;
            var _event_day = id.Event_Day;
            var _event_desc = id.Event_Description;
            var _event_reg = id.Event_Reg_End_Date;
            var _event_abs = id.Event_Abs_Submission;
            var _con1 = id.Contact1;
            var _con2 = id.Contact2;
            var _name1 = id.Name1;
            var _phone1 = id.Phone1;
            var _name2 = id.Name2;
            var _phone2 = id.Phone2;
            var _event_reg_link = id.Event_Reg_Link;

            if (_event_name == "Innovations" | _event_name == "TechXter")
            {
                NavigationService.Navigate(new Uri("/InoTech.xaml?eName=" + _event_name + "&eDay=" + _event_day + "&eDesc=" + _event_desc + "&eReg=" + _event_reg + "&co1=" + _con1 + "&co2=" + _con2 + "&abs=" + _event_abs + "&nm1=" + _name1 + "&ph1=%2b" + _phone1 + "&nm2=" + _name2 + "&ph2=%2b" +  _phone2 + "&regLink=" + _event_reg_link, UriKind.Relative));
            }
            else
            {
                NavigationService.Navigate(new Uri("/Rest.xaml?eName=" + _event_name + "&eDay=" + _event_day + "&eDesc=" + _event_desc + "&eReg=" + _event_reg + "&co1=" + _con1 + "&co2=" + _con2 + "&nm1=" + _name1 + "&ph1=%2b" + _phone1 + "&nm2=" + _name2 + "&ph2=%2b" + _phone2 + "&regLink=" + _event_reg_link, UriKind.Relative));
            }
        }

        private void OnClick(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Inaug.xaml", UriKind.Relative));
        }

        private void PanoramaItem_Loaded(object sender, RoutedEventArgs e)
        {
            MapsTask mapsTask = new MapsTask();

            //Omit the Center property to use the user's current location.
            //mapsTask.Center = new GeoCoordinate(47.6204, -122.3493);

            mapsTask.SearchTerm = "SIES Graduate School Of Technology, Nerul, Navi Mumbai";
            mapsTask.ZoomLevel = 16;

            mapsTask.Show();
        }

        private void onInterEventExpand(object sender, SelectionChangedEventArgs e)
        {
            InterCollege i = InterCollegeEvents.SelectedItem as InterCollege;
            var _adr = i.Adrenaline;
            var _mis = i.Misc;
            var _sub = i.Subtle_Creativity;
            var _env = i.Envision;

            if (_adr == "Adrenaline")
            {
                NavigationService.Navigate(new Uri("/AdrInter.xaml", UriKind.Relative));
            }
            else if (_mis == "Miscellaneous")
            {
                NavigationService.Navigate(new Uri("/MsInter.xaml", UriKind.Relative));
            }
            else if (_sub == "Subtle")
            {
                NavigationService.Navigate(new Uri("/SubInter.xaml", UriKind.Relative));
            }
            else if (_env == "Envision")
            {
                NavigationService.Navigate(new Uri("/EnvInter.xaml", UriKind.Relative));
            }
        }

        private void onIntraEventExpand(object sender, SelectionChangedEventArgs e)
        {
            EventList ID = IntraCollegeEvents.SelectedItem as EventList;
            var _event_name = ID.Event_Name;
            var _event_day = ID.Event_Day;
            var _event_desc = ID.Event_Description;
            var _name1 = ID.Name1;
            var _phone1 = ID.Phone1;
            var _name2 = ID.Name2;
            var _phone2 = ID.Phone2;
            var _event_reg_link = ID.Event_Reg_Link;

            NavigationService.Navigate(new Uri("/All.xaml?eName=" + _event_name + "&eDay=" + _event_day + "&eDesc=" + _event_desc + "&nm1=" + _name1 + "&ph1=%2b" + _phone1 + "&nm2=" + _name2 + "&ph2=%2b" + _phone2 + "&regLink=" + _event_reg_link, UriKind.Relative));
        }

        private void onWorkshopExpand(object sender, SelectionChangedEventArgs e)
        {
            EventList ID = Workshop.SelectedItem as EventList;
            var _event_name = ID.Event_Name;
            var _event_day = ID.Event_Day;
            var _event_desc = ID.Event_Description;
            var _name1 = ID.Name1;
            var _phone1 = ID.Phone1;
            var _name2 = ID.Name2;
            var _phone2 = ID.Phone2;
            var _event_reg_link = ID.Event_Reg_Link;

            NavigationService.Navigate(new Uri("/All.xaml?eName=" + _event_name + "&eDay=" + _event_day + "&eDesc=" + _event_desc + "&nm1=" + _name1 + "&ph1=%2b" + _phone1 + "&nm2=" + _name2 + "&ph2=%2b" + _phone2 + "&regLink=" + _event_reg_link, UriKind.Relative));
        }

        /*private void onNewsExpand(object sender, SelectionChangedEventArgs e)
        {

        }*/

        private async void refreshClick(object sender, EventArgs e)
        {
            ApplicationBarIconButton btn = (ApplicationBarIconButton)ApplicationBar.Buttons[0];

            var get = App.newDB;


            try
            {
                _progressIndicator = new ProgressIndicator
                {
                    IsIndeterminate = true,
                    Text = "                   Refreshing... Please Wait",
                    IsVisible = true,

                };
                SystemTray.SetIsVisible(this, true);
                SystemTray.SetProgressIndicator(this, _progressIndicator);
                SystemTray.SetOpacity(this, 0);
                SystemTray.SetForegroundColor(this, Colors.Black);

                response = await HttpPost.Httppost();

                dynamic objects = JsonConvert.DeserializeObject(response);

                var refresh = App.newDB;

                using (var drop = refresh.Prepare("DROP TABLE cog_eventdetails"))
                {
                    drop.Step();
                }

                using (var drop = refresh.Prepare("DROP TABLE cog_eventdetails_inter"))
                {
                    drop.Step();
                }

                using (var drop = refresh.Prepare("DROP TABLE cog_eventdetails_intra"))
                {
                    drop.Step();
                }

                using (var drop = refresh.Prepare("DROP TABLE cog_eventdetails_workshop"))
                {
                    drop.Step();
                }
                
                string table = @"CREATE TABLE IF NOT EXISTS cog_eventdetails(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "event_name VARCHAR DEFAULT NULL," +
                "event_time VARCHAR DEFAULT NULL," +
                "day_number VARCHAR DEFAULT NULL," +
                "day VARCHAR DEFAULT NULL," +
                "event_date VARCHAR DEFAULT NULL," +
                "event_day VARCHAR DEFAULT NULL," +
                "event_level VARCHAR DEFAULT NULL," +
                "event_type VARCHAR DEFAULT NULL," +
                "event_description VARCHAR(2000) DEFAULT NULL," +
                "event_reg_end_date VARCHAR DEFAULT NULL," +
                "event_abs_submission VARCHAR DEFAULT NULL," +
                "event_council VARCHAR DEFAULT NULL," +
                "contact1 VARCHAR DEFAULT NULL," +
                "contact2 VARCHAR DEFAULT NULL,"+
                "name1 VARCHAR DEFAULT NULL," +
                "phone1 VARCHAR DEFAULT NULL," +
                "name2 VARCHAR DEFAULT NULL," +
                "phone2 VARCHAR DEFAULT NULL," +
                "event_reg_link VARCHAR DEFAULT NULL);";

                using (var newTable = refresh.Prepare(table))
                {
                    newTable.Step();
                }

                string interTable = @"CREATE TABLE IF NOT EXISTS cog_eventdetails_inter(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "event_name VARCHAR DEFAULT NULL," +
            "event_time VARCHAR DEFAULT NULL," +
            "day_number VARCHAR DEFAULT NULL," +
            "day VARCHAR DEFAULT NULL," +
            "event_date VARCHAR DEFAULT NULL," +
            "event_day VARCHAR DEFAULT NULL," +
            "event_type VARCHAR DEFAULT NULL," +
            "event_description VARCHAR(2000) DEFAULT NULL," +
            "event_reg_link VARCHAR DEFAULT NULL," +
            "name1 VARCHAR DEFAULT NULL," +
            "phone1 VARCHAR DEFAULT NULL," +
            "name2 VARCHAR DEFAULT NULL," +
            "phone2 VARCHAR DEFAULT NULL);";

                using (var que = refresh.Prepare(interTable))
                {
                    que.Step();
                }

                string intraTable = @"CREATE TABLE IF NOT EXISTS cog_eventdetails_intra(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "event_name VARCHAR DEFAULT NULL," +
                "event_time VARCHAR DEFAULT NULL," +
                "day_number VARCHAR DEFAULT NULL," +
                "day VARCHAR DEFAULT NULL," +
                "event_date VARCHAR DEFAULT NULL," +
                "event_day VARCHAR DEFAULT NULL," +
                "event_type VARCHAR DEFAULT NULL," +
                "event_description VARCHAR(2000) DEFAULT NULL," +
                "event_reg_link VARCHAR DEFAULT NULL," +
                "name1 VARCHAR DEFAULT NULL," +
                "phone1 VARCHAR DEFAULT NULL," +
                "name2 VARCHAR DEFAULT NULL," +
                "phone2 VARCHAR DEFAULT NULL);";

                using (var q = refresh.Prepare(intraTable))
                {
                    q.Step();
                }

                string workshopTable = @"CREATE TABLE IF NOT EXISTS cog_eventdetails_workshop(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "event_name VARCHAR DEFAULT NULL," +
                "event_time VARCHAR DEFAULT NULL," +
                "day_number VARCHAR DEFAULT NULL," +
                "day VARCHAR DEFAULT NULL," +
                "event_date VARCHAR DEFAULT NULL," +
                "event_day VARCHAR DEFAULT NULL," +
                "event_type VARCHAR DEFAULT NULL," +
                "event_description VARCHAR(2000) DEFAULT NULL," +
                "event_reg_link VARCHAR DEFAULT NULL," +
                "name1 VARCHAR DEFAULT NULL," +
                "phone1 VARCHAR DEFAULT NULL," +
                "name2 VARCHAR DEFAULT NULL," +
                "phone2 VARCHAR DEFAULT NULL);";

                using (var qu = refresh.Prepare(workshopTable))
                {
                    qu.Step();
                }

                foreach (var o in objects)
                {
                    using (var insert = get.Prepare("INSERT INTO cog_eventdetails (event_name, event_time, day_number, day, event_date, event_day, event_level, event_type, event_description, event_reg_end_date, event_abs_submission, event_council, contact1, contact2, name1, phone1, name2, phone2, event_reg_link) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                    {
                        
                        KEY_EVENT_NAME = o["event_name"];
                        insert.Bind(1, KEY_EVENT_NAME);
                        KEY_EVENT_TIME = o["event_time"];
                        insert.Bind(2, KEY_EVENT_TIME);
                        KEY_DAY_NUMBER = o["day_number"];
                        insert.Bind(3, KEY_DAY_NUMBER);
                        KEY_DAY = o["day"];
                        insert.Bind(4, KEY_DAY);
                        KEY_EVENT_DATE = o["event_date"];
                        insert.Bind(5, KEY_EVENT_DATE);
                        KEY_EVENT_DAY = o["event_day"];
                        insert.Bind(6, KEY_EVENT_DAY);
                        KEY_EVENT_LEVEL = o["event_level"];
                        insert.Bind(7, KEY_EVENT_LEVEL);
                        KEY_EVENT_TYPE = o["event_type"];
                        insert.Bind(8, KEY_EVENT_TYPE);
                        KEY_EVENT_DESC = o["event_description"];
                        insert.Bind(9, KEY_EVENT_DESC);
                        KEY_EVENT_REG_END_DATE = o["event_reg_end_date"];
                        insert.Bind(10, KEY_EVENT_REG_END_DATE);
                        KEY_EVENT_ABS_SUB = o["event_abs_submission"];
                        insert.Bind(11, KEY_EVENT_ABS_SUB);
                        KEY_EVENT_COUNCIL = o["event_council"];
                        insert.Bind(12, KEY_EVENT_COUNCIL);
                        KEY_CONTACT_ONE = o["contact1"];
                        insert.Bind(13, KEY_CONTACT_ONE);
                        KEY_CONTACT_TWO = o["contact2"];
                        insert.Bind(14, KEY_CONTACT_TWO);
                        KEY_NAME_ONE = o["name1"];
                        insert.Bind(15, KEY_NAME_ONE);
                        KEY_PHONE_ONE = o["phone1"];
                        insert.Bind(16, KEY_PHONE_ONE);
                        KEY_NAME_TWO = o["name2"];
                        insert.Bind(17, KEY_NAME_TWO);
                        KEY_PHONE_TWO = o["phone2"];
                        insert.Bind(18, KEY_PHONE_TWO);
                        KEY_EVENT_REG_LINK = o["event_reg_link"];
                        insert.Bind(19, KEY_EVENT_REG_LINK);
                        insert.Step();
                    } 
                }

                string inter = await HttpPost.HttppostInter();
                dynamic obj = JsonConvert.DeserializeObject(inter);

                foreach (var andar in obj)
                {
                    using (var put = get.Prepare("INSERT INTO cog_eventdetails_inter (event_name, event_time, day_number, day, event_date, event_day, event_type, event_description, event_reg_link, name1, phone1, name2, phone2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                    {
                        KEY_EVENT_NAME = andar["event_name"];
                        put.Bind(1, KEY_EVENT_NAME);
                        KEY_EVENT_TIME = andar["event_time"];
                        put.Bind(2, KEY_EVENT_TIME);
                        KEY_DAY_NUMBER = andar["day_number"];
                        put.Bind(3, KEY_DAY_NUMBER);
                        KEY_DAY = andar["day"];
                        put.Bind(4, KEY_DAY);
                        KEY_EVENT_DATE = andar["event_date"];
                        put.Bind(5, KEY_EVENT_DATE);
                        KEY_EVENT_DAY = andar["event_day"];
                        put.Bind(6, KEY_EVENT_DAY);
                        KEY_EVENT_TYPE = andar["event_type"];
                        put.Bind(7, KEY_EVENT_TYPE);
                        KEY_EVENT_DESC = andar["event_description"];
                        put.Bind(8, KEY_EVENT_DESC);
                        KEY_EVENT_REG_LINK = andar["event_reg_link"];
                        put.Bind(9, KEY_EVENT_REG_LINK);
                        KEY_NAME_ONE = andar["name1"];
                        put.Bind(10, KEY_NAME_ONE);
                        KEY_PHONE_ONE = andar["phone1"];
                        put.Bind(11, KEY_PHONE_ONE);
                        KEY_NAME_TWO = andar["name2"];
                        put.Bind(12, KEY_NAME_TWO);
                        KEY_PHONE_TWO = andar["phone2"];
                        put.Bind(13, KEY_PHONE_TWO);
                        put.Step();
                    }
                }

                string intra = await HttpPost.HttppostIntra();
                dynamic ob = JsonConvert.DeserializeObject(intra);

                foreach (var bahar in ob)
                {
                    using (var put = get.Prepare("INSERT INTO cog_eventdetails_intra (event_name, event_time, day_number, day, event_date, event_day, event_type, event_description, event_reg_link, name1, phone1, name2, phone2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                    {
                        KEY_EVENT_NAME = bahar["event_name"];
                        put.Bind(1, KEY_EVENT_NAME);
                        KEY_EVENT_TIME = bahar["event_time"];
                        put.Bind(2, KEY_EVENT_TIME);
                        KEY_DAY_NUMBER = bahar["day_number"];
                        put.Bind(3, KEY_DAY_NUMBER);
                        KEY_DAY = bahar["day"];
                        put.Bind(4, KEY_DAY);
                        KEY_EVENT_DATE = bahar["event_date"];
                        put.Bind(5, KEY_EVENT_DATE);
                        KEY_EVENT_DAY = bahar["event_day"];
                        put.Bind(6, KEY_EVENT_DAY);
                        KEY_EVENT_TYPE = bahar["event_type"];
                        put.Bind(7, KEY_EVENT_TYPE);
                        KEY_EVENT_DESC = bahar["event_description"];
                        put.Bind(8, KEY_EVENT_DESC);
                        KEY_EVENT_REG_LINK = bahar["event_reg_link"];
                        put.Bind(9, KEY_EVENT_REG_LINK);
                        KEY_NAME_ONE = bahar["name1"];
                        put.Bind(10, KEY_NAME_ONE);
                        KEY_PHONE_ONE = bahar["phone1"];
                        put.Bind(11, KEY_PHONE_ONE);
                        KEY_NAME_TWO = bahar["name2"];
                        put.Bind(12, KEY_NAME_TWO);
                        KEY_PHONE_TWO = bahar["phone2"];
                        put.Bind(13, KEY_PHONE_TWO);
                        put.Step();
                    }
                }

                string workshop = await HttpPost.HttppostWork();
                dynamic objct = JsonConvert.DeserializeObject(workshop);

                foreach (var work in objct)
                {
                    using (var put = get.Prepare("INSERT INTO cog_eventdetails_workshop(event_name, event_time, day_number, day, event_date, event_day, event_type, event_description, event_reg_link, name1, phone1, name2, phone2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"))
                    {
                        KEY_EVENT_NAME = work["event_name"];
                        put.Bind(1, KEY_EVENT_NAME);
                        KEY_EVENT_TIME = work["event_time"];
                        put.Bind(2, KEY_EVENT_TIME);
                        KEY_DAY_NUMBER = work["day_number"];
                        put.Bind(3, KEY_DAY_NUMBER);
                        KEY_DAY = work["day"];
                        put.Bind(4, KEY_DAY);
                        KEY_EVENT_DATE = work["event_date"];
                        put.Bind(5, KEY_EVENT_DATE);
                        KEY_EVENT_DAY = work["event_day"];
                        put.Bind(6, KEY_EVENT_DAY);
                        KEY_EVENT_TYPE = work["event_type"];
                        put.Bind(7, KEY_EVENT_TYPE);
                        KEY_EVENT_DESC = work["event_description"];
                        put.Bind(8, KEY_EVENT_DESC);
                        KEY_EVENT_REG_LINK = work["event_reg_link"];
                        put.Bind(9, KEY_EVENT_REG_LINK);
                        KEY_NAME_ONE = work["name1"];
                        put.Bind(10, KEY_NAME_ONE);
                        KEY_PHONE_ONE = work["phone1"];
                        put.Bind(11, KEY_PHONE_ONE);
                        KEY_NAME_TWO = work["name2"];
                        put.Bind(12, KEY_NAME_TWO);
                        KEY_PHONE_TWO = work["phone2"];
                        put.Bind(13, KEY_PHONE_TWO);
                        put.Step();
                    }
                }

                Int64 LastID;
                using (var last = get.Prepare("SELECT _id FROM cog_eventdetails ORDER BY _id DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }
                
                List<EventList> events = new List<EventList>();
                int i = 1;
                string s = "";
                while (i <= LastID)
                {
                    using (var getList = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_end_date, event_abs_submission, contact1, contact2, name1, phone1, name2, phone2 FROM cog_eventdetails WHERE _id = ?"))
                    {
                        getList.Bind(1, i);
                        getList.Step();
                        KEY_EVENT_NAME = (string)getList[1];
                        switch (KEY_EVENT_NAME)
                        {
                            case "TechnoKrieg":
                                s = "/Assets/Event/Event_TechnoKrieg.png";
                                break;

                            case "Innovations":
                                s = "/Assets/Event/Event_Innovations.png";
                                break;

                            case "Matrix":
                                s = "/Assets/Event/Event_Matrix.png";
                                break;

                            case "WebWeaWerz":
                                s = "/Assets/Event/Event_WWW.png";
                                break;

                            case "TechXter":
                                s = "/Assets/Event/Event_TechXter.png";
                                break;

                            case "IQ":
                                s = "/Assets/Event/Event_IQ.png";
                                break;

                            case "Squabble":
                                s = "/Assets/Event/Event_Squabble.png";
                                break;

                            default:
                                s = "/Assets/Event/Event_Hack.png";
                                break;
                        }
                        events.Add(new EventList() { Event_Name = (string)getList[1], Event_Day = (string)getList[2], Event_Description = (string)getList[3], Event_Reg_End_Date = (string)getList[4], Event_Abs_Submission = (string)getList[5], Contact1 = (string)getList[6], Contact2 = (string)getList[7], Name1 = (string)getList[8], Phone1 = (string)getList[9], Name2 = (string)getList[10], Phone2 = (string)getList[11], Event_Reg_Link = (string)getList[12], img = s });
                        i++;
                    }
                }
                EventsView.ItemsSource = events;

                using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_workshop ORDER BY _id DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }

                List<EventList> InterDB = new List<EventList>();
                int j = 1;
                string h = "";
                while (j <= LastID)
                {
                    using (var List = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_link, name1, phone1, name2, phone2 FROM cog_eventdetails_workshop WHERE _id = ?"))
                    {
                        List.Bind(1, j);
                        List.Step();
                        KEY_EVENT_NAME = (string)List[1];
                        switch (KEY_EVENT_NAME)
                        {
                            case "Python":
                                h = "/Assets/Workshop/Python_Workshop.png";
                                break;

                            case "Photoshop":
                                h = "/Assets/Workshop/Photoshop_Workshop.png";
                                break;

                            case "Php/SQL":
                                h = "/Assets/Workshop/Php_Workshop.png";
                                break;

                            case "Arduino":
                                h = "/Assets/Workshop/Arduino_Workshop.png";
                                break;

                            default:
                                h = null;
                                break;
                        }
                        if (h == null)
                        {

                        }
                        else
                        {
                            InterDB.Add(new EventList() { Event_Name = (string)List[1], Event_Day = (string)List[2], Event_Description = (string)List[3], Event_Reg_Link = (string)List[4], Name1 = (string)List[5], Phone1 = (string)List[6], Name2 = (string)List[7], Phone2 = (string)List[8], img = h });
                        }
                        j++;
                    }
                }
                Workshop.ItemsSource = InterDB;

                using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_intra ORDER BY _id DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }

                List<EventList> IntraDB = new List<EventList>();
                int l = 1;
                string a = "";
                while (l <= LastID)
                {
                    using (var List = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_link, name1, phone1, name2, phone2 FROM cog_eventdetails_intra WHERE _id = ?"))
                    {
                        List.Bind(1, l);
                        List.Step();
                        KEY_EVENT_NAME = (string)List[1];
                        switch (KEY_EVENT_NAME)
                        {
                            case "Google Whacking":
                                a = "/Assets/Intra/GoogleWhacking_Icon.png";
                                break;

                            case "Code Warz":
                                a = "/Assets/Intra/CodeWars_Icon.png";
                                break;

                            case "PhotoEditor":
                                a = "/Assets/Intra/PhotoEditor_Icon.png";
                                break;

                            case "Technolution":
                                a = "/Assets/Intra/TPP_Icon.png";
                                break;

                            case "BlitzKrieg":
                                a = "/Assets/Intra/Blitzkrieg_Icon.png";
                                break;

                            default:
                                a = null;
                                break;
                        }
                        if (a == null)
                        {

                        }
                        else
                        {
                            IntraDB.Add(new EventList() { Event_Name = (string)List[1], Event_Day = (string)List[2], Event_Description = (string)List[3], Event_Reg_Link = (string)List[4], Name1 = (string)List[5], Phone1 = (string)List[6], Name2 = (string)List[7], Phone2 = (string)List[8], img = a });
                        }
                        l++;
                    }
                }
                IntraCollegeEvents.ItemsSource = IntraDB;

                _progressIndicator.IsVisible = false;
                SystemTray.SetIsVisible(this, false);
            }
            catch (System.Net.WebException)
            {
                ToastPrompt toast1 = new ToastPrompt();
                toast1.Message = "Please check your internet connection";
                toast1.Show();

                Int64 LastID;
                using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_intra ORDER BY _id DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }

                List<EventList> events = new List<EventList>();
                int i = 1;
                string s = "";
                while (i <= LastID)
                {
                    using (var getList = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_end_date, event_abs_submission, contact1, contact2, name1, phone1, name2, phone2 FROM cog_eventdetails WHERE _id = ?"))
                    {
                        getList.Bind(1, i);
                        getList.Step();
                        KEY_EVENT_NAME = (string)getList[1];
                        switch (KEY_EVENT_NAME)
                        {
                            case "TechnoKrieg":
                                s = "/Assets/Event/Event_TechnoKrieg.png";
                                break;

                            case "Innovations":
                                s = "/Assets/Event/Event_Innovations.png";
                                break;

                            case "Matrix":
                                s = "/Assets/Event/Event_Matrix.png";
                                break;

                            case "WebWeaWerz":
                                s = "/Assets/Event/Event_WWW.png";
                                break;

                            case "TechXter":
                                s = "/Assets/Event/Event_TechXter.png";
                                break;

                            case "IQ":
                                s = "/Assets/Event/Event_IQ.png";
                                break;

                            case "Squabble":
                                s = "/Assets/Event/Event_Squabble.png";
                                break;

                            default:
                                s = null;
                                break;
                        }
                        if (s == null)
                        {
                            //events.Add(new EventList() { Event_Name = (string)getList[1], Event_Day = (string)getList[2], Event_Description = (string)getList[3], Event_Reg_End_Date = (string)getList[4], Event_Abs_Submission = (string)getList[5], Contact1 = (string)getList[6], Contact2 = (string)getList[7]});
                        }
                        else
                        {
                            events.Add(new EventList() { Event_Name = (string)getList[1], Event_Day = (string)getList[2], Event_Description = (string)getList[3], Event_Reg_End_Date = (string)getList[4], Event_Abs_Submission = (string)getList[5], Contact1 = (string)getList[6], Contact2 = (string)getList[7], Name1 = (string)getList[8], Phone1 = (string)getList[9], Name2 = (string)getList[10], Phone2 = (string)getList[11], Event_Reg_Link = (string)getList[12], img = s });
                        }
                        i++;
                    }
                }
                EventsView.ItemsSource = events;

                using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_intra ORDER BY _id DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }

                List<EventList> InterDB = new List<EventList>();
                int j = 1;
                string h = "";
                while (j < LastID)
                {
                    using (var List = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_link, name1, phone1, name2, phone2 FROM cog_eventdetails_workshop WHERE _id = ?"))
                    {
                        List.Bind(1, j);
                        List.Step();
                        KEY_EVENT_NAME = (string)List[1];
                        switch (KEY_EVENT_NAME)
                        {
                            case "Python":
                                h = "/Assets/Workshop/Python_Workshop.png";
                                break;

                            case "Photoshop":
                                h = "/Assets/Workshop/Photoshop_Workshop.png";
                                break;

                            case "Php/SQL":
                                h = "/Assets/Workshop/Php_Workshop.png";
                                break;

                            case "Arduino":
                                h = "/Assets/Workshop/Arduino_Workshop.png";
                                break;

                            default:
                                h = null;
                                break;
                        }
                        if (h == null)
                        {

                        }
                        else
                        {
                            InterDB.Add(new EventList() { Event_Name = (string)List[1], Event_Day = (string)List[2], Event_Description = (string)List[3], Event_Reg_Link = (string)List[4], Name1 = (string)List[5], Phone1 = (string)List[6], Name2 = (string)List[7], Phone2 = (string)List[8], img = h });
                        }
                        j++;
                    }
                }
                Workshop.ItemsSource = InterDB;

                using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_intra ORDER BY _id DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }

                List<EventList> IntraDB = new List<EventList>();
                int l = 1;
                string a = "";
                while (j < LastID)
                {
                    using (var List = get.Prepare("SELECT _id, event_name, event_day, event_description, event_reg_link, name1, phone1, name2, phone2 FROM cog_eventdetails_intra WHERE _id = ?"))
                    {
                        List.Bind(1, l);
                        List.Step();
                        KEY_EVENT_NAME = (string)List[1];
                        switch (KEY_EVENT_NAME)
                        {
                            case "Python":
                                a = "/Assets/Workshop/Python_Workshop.png";
                                break;

                            case "Photoshop":
                                a = "/Assets/Workshop/Photoshop_Workshop.png";
                                break;

                            case "Php/SQL":
                                a = "/Assets/Workshop/Php_Workshop.png";
                                break;

                            case "Arduino":
                                a = "/Assets/Workshop/Arduino_Workshop.png";
                                break;

                            default:
                                a = null;
                                break;
                        }
                        if (a == null)
                        {

                        }
                        else
                        {
                            IntraDB.Add(new EventList() { Event_Name = (string)List[1], Event_Day = (string)List[2], Event_Description = (string)List[3], Event_Reg_Link = (string)List[4], Name1 = (string)List[5], Phone1 = (string)List[6], Name2 = (string)List[7], Phone2 = (string)List[8], img = h });
                        }
                        j++;
                    }
                }
                IntraCollegeEvents.ItemsSource = IntraDB;
                _progressIndicator.IsVisible = false;
                SystemTray.SetIsVisible(this, false);
                // }
            }
        }

       /* private void Panorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ApplicationBar.IsVisible = false;
            ApplicationBar.IsVisible = true;
        }
        */
    }
}