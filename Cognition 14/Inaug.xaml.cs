﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Coding4Fun.Toolkit.Controls;

namespace Cognition
{
    public partial class Inaug : PhoneApplicationPage
    {
        public Inaug()
        {
            InitializeComponent();

            Event_Name.Text = "Welcome!";
            Event_Desc.Text = "Cognition is back again with a plethora of events, specifically to inspire people to showcase their creativity through their expertise. We have come a long way witnessing creations, and have been working real hard to involve students to push their limits and prove their technical,managerial and leadership skills. The event starts on Engineer's day, September 15th. We invite you to the participate and experience, the inventions and discoveries of tomorrow.";
            Event_Day.Text = "Day 1, 9:00 am";
            name1.Text = "Shashank Krishnan";
            name2.Text = "Pramit Kumar";

            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton share = new ApplicationBarIconButton();
            share.IconUri = new Uri("/Assets/AppBar/share.png", UriKind.Relative);
            share.Text = "share";
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.Buttons.Add(share);
            share.Click += new EventHandler(onShareClick);
        }

        private void phone1_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = "+91 98 67 145103";
            phoneCallTask.DisplayName = "Shashank Krishnan";

            phoneCallTask.Show();
        }

        private void msg1_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();

            smsComposeTask.To = "+91 98 67 145103";

            smsComposeTask.Show();
        }

        private void phone2_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = "+91 99 30 175997";
            phoneCallTask.DisplayName = "Pramit Kumar";

            phoneCallTask.Show();
        }

        private void msg2_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();

            smsComposeTask.To = "+91 99 30 175997";

            smsComposeTask.Show();
        }

        private void onShareClick(object sender, EventArgs e)
        {
            /*ToastPrompt toast1 = new ToastPrompt();
            toast1.Message = "Content copied to clipboard. Happy Sharing!";
            toast1.Show();*/
            MessageBox.Show("Content copied to clipboard. Now paste it and share with anyone, anywhere!");

            Clipboard.SetText(Event_Name.Text + "\n" + Event_Day.Text + "\n\n" + Event_Desc.Text + "\n\n" + "Contact Us" + "\n\n" + "Shashank Krishnan : +91 98 67 145103" + "\n\n" + "Pramit Kumar : +91 99 30 175997");
        }
    }
}