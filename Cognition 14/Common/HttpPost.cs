﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Microsoft.Phone.Shell;

namespace Cognition.Common
{
    class HttpPost
    {

        public async static Task<string> Httppost()
        {

            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = HttpWebRequest.Create("http://speed.byethost16.com/cognition2014/cognition2014.php") as HttpWebRequest;

            // Set the Method property of the request to POST.
            request.Method = "POST";

            // Create POST data and convert it to a byte array.
            String postData = "";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            //Write data into a stream
            using (var stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            // Pick up the response:
            string result = null;
            using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }

            return result;

        }

        public async static Task<string> HttppostInter()
        {

            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = HttpWebRequest.Create("http://speed.byethost16.com/cognition2014/inter.php") as HttpWebRequest;

            // Set the Method property of the request to POST.
            request.Method = "POST";

            // Create POST data and convert it to a byte array.
            String postData = "";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            //Write data into a stream
            using (var stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            // Pick up the response:
            string result = null;
            using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }

            return result;

        }

        public async static Task<string> HttppostIntra()
        {

            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = HttpWebRequest.Create("http://speed.byethost16.com/cognition2014/intra.php") as HttpWebRequest;

            // Set the Method property of the request to POST.
            request.Method = "POST";

            // Create POST data and convert it to a byte array.
            String postData = "";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            //Write data into a stream
            using (var stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            // Pick up the response:
            string result = null;
            using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }

            return result;
        }

        public async static Task<string> HttppostWork()
        {

            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = HttpWebRequest.Create("http://speed.byethost16.com/cognition2014/workshop.php") as HttpWebRequest;

            // Set the Method property of the request to POST.
            request.Method = "POST";

            // Create POST data and convert it to a byte array.
            String postData = "";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            //Write data into a stream
            using (var stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            // Pick up the response:
            string result = null;
            using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }

            return result;

        }
    }
}