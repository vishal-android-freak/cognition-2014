﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Coding4Fun.Toolkit.Controls;

namespace Cognition_14
{
    public partial class All : PhoneApplicationPage
    {
        public All()
        {
            InitializeComponent();

            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton share = new ApplicationBarIconButton();
            share.IconUri = new Uri("/Assets/AppBar/share.png", UriKind.Relative);
            share.Text = "share";
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.Buttons.Add(share);
            share.Click += new EventHandler(onShareClick);
        }

        string eName, eDay, eDesc, nm1, ph1, nm2, ph2, phone1, phone2, reg, regLink;
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.TryGetValue("eName", out eName) | NavigationContext.QueryString.TryGetValue("eDay", out eDay) | NavigationContext.QueryString.TryGetValue("eDesc", out eDesc) | NavigationContext.QueryString.TryGetValue("nm1", out nm1) | NavigationContext.QueryString.TryGetValue("ph1", out ph1) | NavigationContext.QueryString.TryGetValue("nm2", out nm2) | NavigationContext.QueryString.TryGetValue("ph2", out ph2) | NavigationContext.QueryString.TryGetValue("regLink", out reg))
            {
                Event_Name.Text = eName;
                Event_Day.Text = eDay;
                Event_Desc.Text = eDesc;
                name1.Text = nm1;
                name2.Text = nm2;
                phone1 = ph1;
                phone2 = ph2;
                regLink = reg;
            }

            if (regLink == "")
            {
                ButtonReg.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void phone1_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = phone1;
            phoneCallTask.DisplayName = name1.Text;

            phoneCallTask.Show();
        }

        private void msg1_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();

            smsComposeTask.To = phone1;

            smsComposeTask.Show();
        }

        private void phone2_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = phone2;
            phoneCallTask.DisplayName = name2.Text;

            phoneCallTask.Show();
        }

        private void msg2_tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();

            smsComposeTask.To = phone2;

            smsComposeTask.Show();
        }

        private void onRegClick(object sender, RoutedEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            if (regLink != "NULL")
            {
                webBrowserTask.Uri = new Uri(regLink, UriKind.Absolute);
                webBrowserTask.Show();
            }
            else
            {
                ToastPrompt toast1 = new ToastPrompt();
                toast1.Message = @"No registration form available. Will be up soon!";
                toast1.Show();
            }
        }

        private void onShareClick(object sender, EventArgs e)
        {
            /*ToastPrompt toast1 = new ToastPrompt();
            toast1.Message = "Content copied to clipboard. Happy Sharing!";
            toast1.Show();*/
            MessageBox.Show("Content copied to clipboard. Now paste it and share with anyone, anywhere!");

            if (regLink == "")
            {
                Clipboard.SetText(eName + "\n" + eDay + "\n\n" + eDesc + "\n\n" + "\n\n" + "Contact Us :" + "\n" + "-" + nm1 + ": " + ph1 + "\n" + "-" + nm2 + ": " + ph2 + "\n\n" + "Download Cognition 2014 for Android : https://goo.gl/cDgfKj" + "\n\n" + "Download Cognition 2014 for Windows : https://goo.gl/73aW2u");
            }
            else
            {
                Clipboard.SetText(eName + "\n" + eDay + "\n\n" + eDesc + "\n\n" + "\n\n" + "Contact Us :" + "\n" + "-" + nm1 + ": " + ph1 + "\n" + "-" + nm2 + ": " + ph2 + "\n\n" + "Online Registration Link :" + "\n" + "-" + regLink + "\n\n" + "Download Cognition 2014 for Android : https://goo.gl/cDgfKj" + "\n\n" + "Download Cognition 2014 for Windows : https://goo.gl/73aW2u");
            }
        }
    }
}