﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Coding4Fun.Toolkit.Controls;

namespace Cognition_14
{
    public partial class SubInter : PhoneApplicationPage
    {

        public String P_KEY = "_id";
        public String KEY_EVENT_NAME;
        public String KEY_EVENT_TIME = "event_time";
        public String KEY_DAY_NUMBER = "day_number";
        public String KEY_DAY = "day";
        public String KEY_EVENT_DATE = "event_date";
        public String KEY_EVENT_DAY = "event_day";
        public String KEY_EVENT_LEVEL = "event_level";
        public String KEY_EVENT_TYPE = "event_type";
        public String KEY_EVENT_DESC = "event_description";
        public String KEY_EVENT_REG_END_DATE = "event_reg_end_date";
        public String KEY_EVENT_ABS_SUB = "event_abs_submission";
        public String KEY_EVENT_COUNCIL = "event_council";
        public String KEY_CONTACT_ONE = "contact1";
        public String KEY_CONTACT_TWO = "contact2";
        public String KEY_NAME_ONE = "name1";
        public String KEY_PHONE_ONE = "phone1";
        public String KEY_NAME_TWO = "name2";
        public String KEY_PHONE_TWO = "phone2";
        public String KEY_EVENT_REG_LINK = "event_reg_link";

        public SubInter()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var get = App.newDB;

            Int64 LastID;
            using (var last = get.Prepare("SELECT _id FROM cog_eventdetails_inter ORDER BY _id DESC"))
            {
                last.Step();
                LastID = (Int64)last[0];
            }

            List<EventList> events = new List<EventList>();
            int i = 1;
            while (i <= LastID)
            {
                using (var getList = get.Prepare("SELECT _id, event_name, event_day, event_description, name1, phone1, name2, phone2, event_reg_link FROM cog_eventdetails_inter WHERE _id = ?"))
                {
                    getList.Bind(1, i);
                    getList.Step();
                    KEY_EVENT_NAME = (string)getList[1];
                    if (KEY_EVENT_NAME == null)
                    {
                        ToastPrompt toast1 = new ToastPrompt();
                        toast1.Message = "Please check your internet connection";
                        toast1.Show();
                    }
                    else if (KEY_EVENT_NAME == "TechChef" | KEY_EVENT_NAME == "Virtual Sculpture" | KEY_EVENT_NAME == "Fix It Up!")
                    {
                        events.Add(new EventList() { Event_Name = (string)getList[1], Event_Day = (string)getList[2], Event_Description = (string)getList[3], Name1 = (string)getList[4], Phone1 = (string)getList[5], Name2 = (string)getList[6], Phone2 = (string)getList[7], Event_Reg_Link = (string)getList[8] });
                    }
                    i++;

                }
            }
            SubtleView.ItemsSource = events;
        }

        public class EventList
        {
            public string Event_Name { get; set; }
            public string Event_Day { get; set; }
            public string Event_Description { get; set; }
            public string Name1 { get; set; }
            public string Phone1 { get; set; }
            public string Name2 { get; set; }
            public string Phone2 { get; set; }
            public string Event_Reg_Link { get; set; }
        }

        private void SubtleView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EventList ID = SubtleView.SelectedItem as EventList;
            var _event_name = ID.Event_Name;
            var _event_day = ID.Event_Day;
            var _event_desc = ID.Event_Description;
            var _name1 = ID.Name1;
            var _phone1 = ID.Phone1;
            var _name2 = ID.Name2;
            var _phone2 = ID.Phone2;
            var _event_reg_link = ID.Event_Reg_Link;

            NavigationService.Navigate(new Uri("/All.xaml?eName=" + _event_name + "&eDay=" + _event_day + "&eDesc=" + _event_desc + "&nm1=" + _name1 + "&ph1=%2b" + _phone1 + "&nm2=" + _name2 + "&ph2=%2b" + _phone2 + "&regLink=" + _event_reg_link, UriKind.Relative));
        
        }
    }
}